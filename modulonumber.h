#pragma once
#include <iostream>

class modulo_number
{
private:
	double number; // абсолютное значение числа или модуль от числа
public:
	modulo_number(double number = 0); // конструктор с параметром
	modulo_number(const modulo_number &other); // конструктор копирования

	modulo_number operator+(const modulo_number&) const; // перегрузка оператора +
	modulo_number operator+(double) const; // перегрузка оператора +
	modulo_number operator-(const modulo_number&) const; // перегрузка оператора -
	modulo_number operator-(double) const; // перегрузка оператора -
	bool operator==(const modulo_number&) const; // перегрузка оператора ==
	bool operator!=(const modulo_number&) const; // перегрузка оператора !=

	double getNumber() const // получить беззнаковое число
	{
		return number;
	}
};

