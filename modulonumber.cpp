#include "modulonumber.h"

modulo_number::modulo_number(double number)
{
	this->number = number > 0 ? number : -number;
}

modulo_number::modulo_number(const modulo_number &other)
{
	this->number = other.number;
}

modulo_number modulo_number::operator +(const modulo_number &other) const
{
	return modulo_number(number + other.number);
}

modulo_number modulo_number::operator +(double other) const
{
	return modulo_number(number + (other > 0 ? other : -other));
}

modulo_number modulo_number::operator -(const modulo_number &other) const
{
	double result = number - other.number;
	return modulo_number(result > 0 ? result : -result);
}

modulo_number modulo_number::operator -(double other) const
{
	double result = number - other;
	return modulo_number(result > 0 ? result : -result);
}

bool modulo_number::operator ==(const modulo_number &other) const
{
	return other.number == number;
}

bool modulo_number::operator !=(const modulo_number &other) const
{
	return other.number != number;
}
