#include <iostream>
#include "modulonumber.h"
#include <string>

using namespace std;

std::ostream& operator<<(std::ostream &out, const modulo_number &moduloNumber)
{
	out << "modulo_number{number = " << moduloNumber.getNumber() << "}";
	return out;
}

int main()
{
	cout
			<< "Создаем числа: a, b и c, передавая в конструктор значения типа int, float и double, соответственно.\n"
					"Печатаем в консоль значения a, b, c, и видим, что значения представляют собой модули чисел, переданных ранее в конструкторы."
			<< endl;
	modulo_number a = 4;
	modulo_number b = -8.5F;
	modulo_number c = -4.5;
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;
	cout << "Создаем число d, которое явлется результатом сложения a и b."
			<< endl;
	modulo_number d = a + b;
	cout << "d = " << d << endl;
	cout
			<< "Создаем число e, которое явлется модулем результата вычитания a и b."
			<< endl;
	modulo_number e = a - b;
	cout << "e = " << e << endl;
	cout
			<< "Теперь проверяем операторы сложения и вычитания с типами данных int, float и double."
			<< endl;
	cout << "e + 3 = " << e + 3 << endl;
	cout << "e + 3.1F = " << e + 3.1F << endl;
	cout << "e + 3.4 = " << e + 3.4 << endl;
	cout << "e - 3 = " << e - 3 << endl;
	cout << "e - 3.1F = " << e - 3.1F << endl;
	cout << "e - 3.4 = " << e - 3.4 << endl;
	cout << "Теперь проверяем операторы != и == " << endl;
	cout << "c = " << c << endl;
	cout << "e = " << e << endl;
	cout << "e == c: " << (e == c ? "true" : "false") << endl;
	cout << "e != c: " << (e != c ? "true" : "false") << endl;
	cout << "Проверяем работу конструктора копирования." << endl;
	modulo_number t = e;
	cout << "t = e = " << t << endl;

	return 0;
}
